package de.fhdw.integrationsproject.ridesharing.controller.mapper;

import de.fhdw.integrationsproject.ridesharing.controller.dto.RideOfferDTO;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;

public class RideOfferMapper {
	public static RideOfferDTO mapToDTO(RideOffer rideOffer) {
		RideOfferDTO offer = new RideOfferDTO();
		offer.setStartingTime(rideOffer.getStartingTime());
		offer.setStartingPlace(LocationMapper.mapToDTO(rideOffer.getStartingPlace()));
		offer.setDestinationPlace(LocationMapper.mapToDTO(rideOffer.getDestinationPlace()));
		offer.setNumberOfPersons(rideOffer.getRequest().getNumberOfPassengers());
		offer.setId(rideOffer.getId());
		offer.setPrice(rideOffer.getPrice());
		return offer;
	}
}
