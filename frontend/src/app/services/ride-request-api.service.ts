import { Injectable } from '@angular/core';
import { RideRequestDTO } from './transport/riderequestdto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Observable } from 'rxjs';
import {RideOfferDTO} from "./transport/RideOfferDTO";

@Injectable({
  providedIn: 'root'
})

export class RideRequestApiService {
  submitRequest(rideRequest: RideRequestDTO): Observable<RideOfferDTO[]> {
       return this.httpClient.post<RideOfferDTO[]>(`${environment.baseUrl}offering/`, rideRequest);
  }

  constructor(private httpClient: HttpClient) {
   }

  acceptOffer(id: string, accepted: boolean) : Observable<any>{
    return this.httpClient.post<any>(`${environment.baseUrl}offering/${id}`, accepted, {headers: new HttpHeaders().set("Content-Type","application/json")});
  }
}
