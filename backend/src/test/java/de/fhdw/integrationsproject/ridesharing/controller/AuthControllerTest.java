package de.fhdw.integrationsproject.ridesharing.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhdw.integrationsproject.ridesharing.controller.dto.request.LoginDTO;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import de.fhdw.integrationsproject.ridesharing.security.services.UserDetailsImpl;
import de.fhdw.integrationsproject.ridesharing.security.services.UserDetailsServiceImpl;
import de.fhdw.integrationsproject.ridesharing.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
public class AuthControllerTest {

    private MockMvc mvc;

    private static final ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private AuthController authController;
    @MockBean
    private UserService userService;
    @Autowired
    private WebApplicationContext context;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void init() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void testRegisterUser() throws Exception {
        User user = new User("hugo", "password", "hugo@balder.de");
        String json = mapper.writeValueAsString(user);

        when(userService.createUser(ArgumentMatchers.any())).thenReturn(user);
        mvc.perform(MockMvcRequestBuilders
                .post("/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("User registered successfully!"));
    }

    @Test
    public void testAuthenticateUser() throws Exception {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername("hugo");
        loginDTO.setPassword("password");

        User user = new User("hugo", passwordEncoder.encode("password"), "hugo@email.de");
        user.addRole(new CustomerRole());

        when(userDetailsService.loadUserByUsername(ArgumentMatchers.any())).thenReturn(UserDetailsImpl.build(user));

        String json = mapper.writeValueAsString(loginDTO);

        mvc.perform(MockMvcRequestBuilders
                .post("/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("hugo"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email").value("hugo@email.de"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.roles.[0]").value("CustomerRole"));
    }
}