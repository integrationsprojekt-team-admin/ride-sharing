package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.exceptions.*;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Ride;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import de.fhdw.integrationsproject.ridesharing.model.offerState.RideOfferState;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class OfferingServiceTest {

	@Autowired
	private OfferingService offeringService;

	@Autowired
	private UserService userService;

	private RideRequest rideRequest;
	private RideOffer rideOffer;
	private User user;

	@BeforeEach
	void setUp() {
		Location start = new Location(52.36619938342916, 9.77279820758798);
		Location destination = new Location(52.37640931562144, 9.740794773412025);

		Ride ride = new Ride(start, destination);
		CustomerRole customerRole = new CustomerRole();
		rideRequest = new RideRequest(ride, LocalDateTime.now(), 1);
		rideOffer = new RideOffer(ride, LocalDateTime.now(), 42d, rideRequest);
		rideOffer.setCustomer(customerRole);

		user = new User();
		user.setUsername("user");
		user.setPassword("a");
		user.setEmail("e@mail.de");
		user.addRole(customerRole);

		try {
			this.userService.createUser(user);
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException e) {
			fail(e);
		}
	}

	@Test
	void acceptRequestReturnNotEmptyList() {
		List<RideOffer> rideOffers = this.offeringService.acceptRequest(rideRequest, user);
		assertFalse(rideOffers.isEmpty());
		rideOffers.forEach(offer -> {
			assertTrue(user.getRoles().contains(offer.getCustomer()));
			assertTrue(rideRequest.getStartingTime().isEqual(offer.getStartingTime()) || rideRequest.getStartingTime().isBefore(offer.getStartingTime()));
			assertEquals(rideRequest.getStartingPlace(), offer.getStartingPlace());
			assertEquals(rideRequest.getDestinationPlace(), offer.getDestinationPlace());
		});
	}

	@Test
	void getByIdNotFound() {
		assertThrows(RideOfferNotFoundException.class, () -> this.offeringService.getById(UUID.randomUUID()));
	}

	@Test
	void getById() {
		List<RideOffer> rideOffers = this.offeringService.acceptRequest(rideRequest, user);
		rideOffers.forEach(offer -> {
			try {
				assertNotNull(this.offeringService.getById(offer.getId()));
			} catch (RideOfferNotFoundException e) {
				fail();
			}
		});
	}

	@Test
	void answerOfferAccepted() throws RideOfferIllegalStateException, UserHasNoPermissionException {
		assertEquals(RideOfferState.RideOfferStateAccepted, offeringService.answerOffer(this.rideOffer, true, user).getState());
	}

	@Test
	void answerOfferNotAccepted() throws RideOfferIllegalStateException, UserHasNoPermissionException {
		assertEquals(RideOfferState.RideOfferStateDeclined, offeringService.answerOffer(this.rideOffer, false, user).getState());
	}
	
	/**
	 * This behavior is also lengthly tested in RideOfferStateMachineTest.
	 */
	@Test
	void answerOfferIllegalState() throws RideOfferIllegalStateException {
		this.rideOffer.accept();
		assertThrows(RideOfferIllegalStateException.class, () -> offeringService.answerOffer(rideOffer, false, user));
	}
}