package de.fhdw.integrationsproject.ridesharing.controller.dto;

import java.time.LocalDateTime;
import java.util.UUID;

public class RideOfferDTO {

	public UUID id;
	public Integer numberOfPersons;
	public LocationDTO startingPlace;
	public LocationDTO destinationPlace;
	public LocalDateTime startingTime;
	public Double price;

	public RideOfferDTO() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setNumberOfPersons(Integer numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public void setStartingPlace(LocationDTO startingPlace) {
		this.startingPlace = startingPlace;
	}

	public void setDestinationPlace(LocationDTO destinationPlace) {
		this.destinationPlace = destinationPlace;
	}

	public LocalDateTime getStartingTime() {
		return startingTime;
	}

	public void setStartingTime(LocalDateTime startingTime) {
		this.startingTime = startingTime;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}
