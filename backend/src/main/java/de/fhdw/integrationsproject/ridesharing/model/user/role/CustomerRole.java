package de.fhdw.integrationsproject.ridesharing.model.user.role;

import de.fhdw.integrationsproject.ridesharing.model.Contract;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import de.fhdw.integrationsproject.ridesharing.visitor.RoleVisitor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;


@Entity
public class CustomerRole extends Role {

	/**
	 * List of rides that the user has requested
	 */
	@OneToMany(
//			mappedBy = "ride_request"
	)
	private List<RideRequest> rideRequests;

	@OneToMany(
//			mappedBy = "ride_offer"
	)
	private List<RideOffer> rideOffers;

	/**
	 * List of contracts that the user has booked
	 */
	@Transient
	//TODO RideRequest zum bentuzer speichern
	private List<Contract> contracts;

	@Override
	public void accept(RoleVisitor roleVisitor) {
		roleVisitor.handleCustomerRole(this);
	}

	public List<RideRequest> getRideRequests() {
		return rideRequests;
	}

	public List<RideOffer> getRideOffers() {
		return rideOffers;
	}


	public void addOffers(List<RideOffer> offers) {
		if (this.rideOffers == null) this.rideOffers = new ArrayList<>();
		this.rideOffers.addAll(offers);
	}

	public void addRequest(RideRequest rideRequest) {
		if (this.rideRequests == null) this.rideRequests = new ArrayList<>();
		this.rideRequests.add(rideRequest);
	}
}
