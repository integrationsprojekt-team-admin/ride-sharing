package de.fhdw.integrationsproject.ridesharing.controller;


import de.fhdw.integrationsproject.ridesharing.controller.dto.RideOfferDTO;
import de.fhdw.integrationsproject.ridesharing.controller.dto.RideRequestDTO;
import de.fhdw.integrationsproject.ridesharing.controller.mapper.RideOfferMapper;
import de.fhdw.integrationsproject.ridesharing.controller.mapper.RideRequestMapper;
import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferIllegalStateException;
import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferNotFoundException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UserHasNoPermissionException;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.security.annotations.IsCustomerUser;
import de.fhdw.integrationsproject.ridesharing.security.services.UserDetailsServiceImpl;
import de.fhdw.integrationsproject.ridesharing.service.OfferingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/offering")
@IsCustomerUser
public class OfferingController {

	@Autowired
	private OfferingService offeringService;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@PostMapping
	public List<RideOfferDTO> acceptRequest(@RequestBody @Valid RideRequestDTO rideRequestDTO) {
		User user = userDetailsService.getUserFromContext();
		RideRequest rideRequest = RideRequestMapper.mapToObject(rideRequestDTO);
		List<RideOffer> rideOffers = this.offeringService.acceptRequest(rideRequest, user);
		return rideOffers.parallelStream().map(RideOfferMapper::mapToDTO).collect(Collectors.toList());
	}

	/**
	 * Is used to answer an offer (i.e. accept or decline).
	 *
	 * @param id       Uniquely identifies the offer, that is answered.
	 * @param accepted Is true, if the user accepted the offer; is false, if the user declined the offer.
	 * @return The offer, which was answered, with an updated state.
	 */
	@PostMapping(value = "/{id}")
	public RideOfferDTO answerOffer(@PathVariable UUID id, @RequestBody Boolean accepted) {
		try {
			RideOffer offer = this.offeringService.getById(id);
			return RideOfferMapper.mapToDTO(this.offeringService.answerOffer(offer, accepted, null));
		} catch (RideOfferNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
		} catch (RideOfferIllegalStateException | UserHasNoPermissionException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
}
