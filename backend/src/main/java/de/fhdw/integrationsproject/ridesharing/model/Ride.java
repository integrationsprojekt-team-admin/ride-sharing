package de.fhdw.integrationsproject.ridesharing.model;

import javax.persistence.*;
import java.util.UUID;

/**
 * Represents a ride from a starting location to a target destination.
 */
@Entity
public class Ride {

	@Id
	@GeneratedValue
	private UUID id;

	/**
	 * This ride's starting location.
	 */
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "lat", column = @Column(name = "start_lat")),
			@AttributeOverride(name = "lng", column = @Column(name = "start_lng")),
	})
	private Location start;

	/**
	 * This ride's target destination.
	 */
	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "lat", column = @Column(name = "destination_lat")),
			@AttributeOverride(name = "lng", column = @Column(name = "destination_lng")),
	})
	private Location destination;


	public Ride(Location start, Location destination) {
		this.start = start;
		this.destination = destination;
	}

	public Ride() {
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Location getStart() {
		return start;
	}


	public void setStart(Location start) {
		this.start = start;
	}


	public Location getDestination() {
		return destination;
	}


	public void setDestination(Location destination) {
		this.destination = destination;
	}
	
	@Override
	public String toString() {
		return "Start: " + this.start.toString() + " Ziel: " + this.destination.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof Ride)) {
			return false;
		}
		Ride other = (Ride) obj;
		return(this.start.equals(other.start) && this.destination.equals(other.destination));
	}

}
