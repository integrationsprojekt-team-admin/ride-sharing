package de.fhdw.integrationsproject.ridesharing;

import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Ride;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;

import java.time.LocalDateTime;

public abstract class RideConstants {

	public static final Location START = new Location(52.37649921834222, 9.741008582843069);
	public static final Location DESTINATION = new Location(52.36622029011966, 9.772736773860924);

	public static final Ride RIDE = new Ride(START, DESTINATION);
	public static final RideRequest RIDE_REQUEST = new RideRequest(RIDE, LocalDateTime.of(2021, 1, 21, 12, 12), 1);

	public static final RideOffer RIDE_OFFER = new RideOffer(RIDE, LocalDateTime.of(2021, 1, 21, 12, 12), 100d, RIDE_REQUEST);
}
