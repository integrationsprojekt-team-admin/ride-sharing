package de.fhdw.integrationsproject.ridesharing.model;

/**
 * A contract is the result of a customer accepting an offer and handles the financial transaction. 
 */
public class Contract {
	
	//TODO: Contracts should have some kind of natural ID like a Buchungsnummer.
	
	/**
	 * The offer which was accepted by a customer and lead to this contract.
	 */
	private RideOffer offer;
	
	@Override
	public String toString() {
		return "Gebucht: " + this.offer.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof Contract)) {
			return false;
		}
		Contract other = (Contract) obj;
		return(this.offer.equals(other.offer));
	}

}
