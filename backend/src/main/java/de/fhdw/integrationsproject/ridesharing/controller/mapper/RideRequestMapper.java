package de.fhdw.integrationsproject.ridesharing.controller.mapper;

import de.fhdw.integrationsproject.ridesharing.controller.dto.RideRequestDTO;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Ride;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import de.fhdw.integrationsproject.ridesharing.service.LocationService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

public class RideRequestMapper {

	public static RideRequest mapToObject(RideRequestDTO rideRequestDTO) {
		Ride ride = new Ride(LocationMapper.mapToObject(rideRequestDTO.getStartingPlace()), LocationMapper.mapToObject(rideRequestDTO.getDestinationPlace()));
		LocalDateTime ldt = rideRequestDTO.getStartingTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		return new RideRequest(ride, ldt, rideRequestDTO.getNumberOfPersons());
	}
}
