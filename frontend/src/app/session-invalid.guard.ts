import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from './services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SessionInvalidGuard implements CanActivate {

  constructor(private tokenStorageService: TokenStorageService, private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!!!this.tokenStorageService.getToken()) {
      return true;
    } else {
      if (this.tokenStorageService.getRole()[0] === 'AdminRole') {
        this.router.navigate(['/admin-view']);
      } else {
        this.router.navigate(['/customer-view']);
      }
    }
  }

}
