package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.exceptions.EmailAdressAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UserNotFoundException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UsernameAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
class UserServiceTest {

	@Autowired
	private UserService userService;

	private User user;

	@BeforeEach
	private void setUp() {
		this.user = new User();
		this.user.setUsername("username");
		this.user.setEmail("email@adresse.de");
		this.user.addRole(new CustomerRole());
		this.userService.encodeAndSetPassword("password", this.user);
	}

	@Test
	void createUserSuccessfully() {
		try {
			User user = userService.createUser(this.user);
			assertNotNull(user.getId());
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException e) {
			fail(e);
		}
	}

	@Test
	void createUserFailUsername() {
		try {
			User user = userService.createUser(this.user);
			assertNotNull(user.getId());
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException e) {
			fail(e);
		}

		User duplicated = new User();
		duplicated.setEmail("email2@adresse.de");
		duplicated.setUsername("username");
		duplicated.addRole(new CustomerRole());

		assertThrows(UsernameAlreadyExistsException.class, () -> userService.createUser(duplicated));
	}

	@Test
	void createUserFailEmail() {
		try {
			User user = userService.createUser(this.user);
			assertNotNull(user.getId());
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException e) {
			fail(e);
		}

		User duplicated = new User();
		duplicated.setEmail("email@adresse.de");
		duplicated.setUsername("username2");
		duplicated.addRole(new CustomerRole());

		assertThrows(EmailAdressAlreadyExistsException.class, () -> userService.createUser(duplicated));
	}


	@Test
	void getUserById() {
		try {
			User user = userService.createUser(this.user);
			assertNotNull(user.getId());
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException e) {
			fail(e);
		}

		try {
			User u = this.userService.getUserById(user.getId());
			assertEquals(user.getId(), u.getId());
		} catch (UserNotFoundException e) {
			fail();
		}
	}

	@Test
	void encodeAndSetPassword() {
		this.userService.encodeAndSetPassword("password", user);
		assertNotEquals(user.getPassword(), "password");
	}
}