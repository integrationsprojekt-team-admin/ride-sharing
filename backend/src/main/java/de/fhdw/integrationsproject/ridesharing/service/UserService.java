package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.exceptions.EmailAdressAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UserNotFoundException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UsernameAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;


	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * creates a new User
	 *
	 * @param user, which should be saved
	 * @return saved User Instance
	 * @throws UsernameAlreadyExistsException,    if username is already in use
	 * @throws EmailAdressAlreadyExistsException, if email is already in use
	 */
	@Transactional
	public User createUser(User user) throws UsernameAlreadyExistsException, EmailAdressAlreadyExistsException {
		if (userRepository.existsByUsername(user.getUsername())) {
			throw new UsernameAlreadyExistsException("User with following username alrady exist: " + user.getUsername());
		}
		if (userRepository.existsByEmail(user.getEmail())) {
			throw new EmailAdressAlreadyExistsException("User with following mail adress already exist: " + user.getEmail());
		}
		return this.userRepository.save(user);
	}

	@Transactional(readOnly = true)
	public User getUserById(UUID uuid) throws UserNotFoundException {
		Optional<User> byId = this.userRepository.findById(uuid);
		if (byId.isEmpty()) throw new UserNotFoundException("User with following id not found: " + uuid.toString());
		return byId.get();
	}

	public void encodeAndSetPassword(String password, User user) {
		user.setPassword(passwordEncoder.encode(password));
	}
}
