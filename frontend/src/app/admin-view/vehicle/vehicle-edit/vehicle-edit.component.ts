import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VehicleDTO } from '../../../services/transport/VehicleDTO';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VehicleService } from '../../../services/vehicle.service';

@Component({
  selector: 'app-vehicle-edit',
  templateUrl: './vehicle-edit.component.html',
  styleUrls: ['./vehicle-edit.component.css']
})
export class VehicleEditComponent implements OnInit {
  formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<VehicleEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VehicleDTO,
    private vehicleService: VehicleService,
  ) {
  }

  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.formGroup = this.formBuilder.group({
      id: [{value: this.data.id, disabled: false}],
      vin: [this.data.vin, [Validators.required,
        Validators.pattern('[A-HJ-NPR-Za-hj-npr-z\\d]{8}[\\dX][A-HJ-NPR-Za-hj-npr-z\\d]{2}\\d{6}')]],
      licensePlate: [this.data.licensePlate, [Validators.required, Validators.pattern('[A-Z]{1,3}-[A-Z]{1,3}[ -]?[0-9]{1,4}')]],
      registrationDate: [this.data.registrationDate, [Validators.required]],
      purchasePrice: [this.data.purchasePrice, [Validators.required, Validators.pattern('[0-9]*.[0-9]*')]],
      brand: [this.data.brand, [Validators.required]],
      model: [this.data.model, [Validators.required]],
      lat: [this.data.lat, [Validators.required, Validators.pattern('-?[0-9]*.[0-9]*')]],
      lng: [this.data.lng, [Validators.required, Validators.pattern('-?[0-9]*.[0-9]*')]],
    });
  }

  submit(value: VehicleDTO): void {
    this.vehicleService.editVehicle(value);
    this.dialogRef.close();
  }
}
