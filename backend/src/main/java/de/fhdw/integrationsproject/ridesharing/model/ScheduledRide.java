package de.fhdw.integrationsproject.ridesharing.model;


import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Represents a specific ride, which is scheduled for a given point in time.
 */
@MappedSuperclass
public abstract class ScheduledRide {

	@Id
	@GeneratedValue
	@Column(name = "id", updatable = false, nullable = false)
	private UUID id;

	/**
	 * The ride, which is scheduled by this instance.
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "RIDE_ID", foreignKey = @ForeignKey(name = "RIDE_ID_FK"))
	private Ride ride;

	/**
	 * The point in time at which the ride is scheduled.
	 */
	@NotNull
	private LocalDateTime startingTime;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	private CustomerRole customer;

	private Integer numberOfPersons;

	public ScheduledRide(Ride ride, LocalDateTime startingTime) {
		this.ride = ride;
		this.startingTime = startingTime;
	}

	public ScheduledRide() {
	}

	public UUID getId() {
		return id;
	}

	public Location getStartingPlace() {
		return this.ride.getStart();
	}

	public Location getDestinationPlace() {
		return this.ride.getDestination();
	}

	public Ride getRide() {
		return ride;
	}

	public void setRide(Ride ride) {
		this.ride = ride;
	}

	public LocalDateTime getStartingTime() {
		return startingTime;
	}

	public Integer getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setStartingTime(LocalDateTime startingTime) {
		this.startingTime = startingTime;
	}

	public CustomerRole getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerRole customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Zeitpunkt: " + this.startingTime.toString() + " " + this.ride.toString();
	}
}
