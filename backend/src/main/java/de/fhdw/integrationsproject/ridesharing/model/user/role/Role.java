package de.fhdw.integrationsproject.ridesharing.model.user.role;


import de.fhdw.integrationsproject.ridesharing.visitor.RoleVisitor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Abstract Role
 * is used for authorisation
 */
@Entity
public abstract class Role {

	@Id
	@GeneratedValue
	private UUID id;

	public abstract void accept(RoleVisitor roleVisitor);

}
