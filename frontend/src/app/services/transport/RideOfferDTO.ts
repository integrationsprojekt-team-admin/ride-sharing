export class RideOfferDTO {
  numberOfPersons: number;
  startingPlace: string;
  targetDestination: string;
  startingTime: Date;
  price: number;
  id: string;
}
