import { browser, by, element, ElementFinder } from 'protractor';

export class VehiclePage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl + 'admin-view');
  }

  async getTitleText(): Promise<string> {
    return element(by.css('div.mat-tab-label-content')).getText();
  }

  async getDialogTitleText(): Promise<string> {
    return element(by.css('.mat-dialog-title')).getText();
  }

  getAddButton(): ElementFinder {
    return element(by.css('#add-button button'));
  }

  async fillVehicleForm(): Promise<any> {
    await element(by.css('#vin')).sendKeys('JH4KA2540GC007745');
    await element(by.css('#licensePlate')).sendKeys('H-AB 1234');
    await element(by.css('#registrationDate')).sendKeys('3/20/2010');
    await element(by.css('#purchasePrice')).sendKeys(14000);
    await element(by.css('#brand')).sendKeys('VW');
    await element(by.css('#model')).sendKeys('Transporter 6.1 Kombi');
    await element(by.css('#lat')).sendKeys(52.376550);
    return element(by.css('#lng')).sendKeys(9.741046);
  }

  fillVehicleUpdateForm(): void {
    element(by.css('#licensePlate')).clear();
    element(by.css('#licensePlate')).sendKeys('B-XY 9876');
  }

  getCreateButton(): ElementFinder {
    return element(by.buttonText('Erstellen'));
  }

  getConfirmEditButton(): ElementFinder {
    return element(by.buttonText('Bearbeiten'));
  }

  async getLicensePlateCell(): Promise<string> {
    return element(by.css('td.cdk-column-licensePlate')).getText();
  }

  getDeleteButton(): ElementFinder {
    return element(by.css('[aria-label^=\"Delete\"]'));
  }

  getConfirmDeleteButton(): ElementFinder {
    return element(by.css('#confirmDelete'));
  }

  getEditButton(): ElementFinder {
    return element(by.css('[aria-label^=\"Edit\"]'));
  }
}
