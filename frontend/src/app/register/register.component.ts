import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { SignupDTO } from '../services/transport/SignupDTO';
import { BehaviorSubject } from 'rxjs';
import { MessageDTO } from '../services/share/message-box/MessageDTO';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.formBuilder.group({
    username: ['', Validators.required],
    email: ['', Validators.email],
    password: ['', Validators.required]
  });

  signupInvalid = false;
  load = false;
  hide = true;
  message = new BehaviorSubject<MessageDTO>(new MessageDTO(''));

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.load = true;
    const {email, username, password} = this.registerForm.getRawValue();
    const signupDTO = new SignupDTO(email, username, password);
    this.authService.register(signupDTO).subscribe(
      data => {
        console.log(data);
        this.message.next(new MessageDTO('Der Nutzer wurde erstellt.', false));
      },
      err => {
        this.signupInvalid = true;
        this.load = false;
        this.message.next(new MessageDTO('Es ist ein Fehler aufgetreten, evtl. existiert bereits für die E-Mail oder dem Nutzer ein Account.', true));
      },
      () => {
        this.load = false;
      }
    );
  }

}
