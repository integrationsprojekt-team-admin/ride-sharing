import { TestBed } from '@angular/core/testing';

import { SessionInvalidGuard } from './session-invalid.guard';

describe('SessionInvalidGuard', () => {
  let guard: SessionInvalidGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SessionInvalidGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
