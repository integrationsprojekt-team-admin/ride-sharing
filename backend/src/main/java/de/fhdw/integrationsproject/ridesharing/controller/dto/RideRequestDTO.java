package de.fhdw.integrationsproject.ridesharing.controller.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Positive;
import java.util.Date;

public class RideRequestDTO {

	@Positive
	public Integer numberOfPersons;

	public String startingPlace;

	public String destinationPlace;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	public Date startingTime;

	public Integer getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(Integer numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public String getStartingPlace() {
		return startingPlace;
	}

	public void setStartingPlace(String startingPlace) {
		this.startingPlace = startingPlace;
	}

	public String getDestinationPlace() {
		return destinationPlace;
	}

	public void setDestinationPlace(String destinationPlace) {
		this.destinationPlace = destinationPlace;
	}

	public Date getStartingTime() {
		return startingTime;
	}

	public void setStartingTime(Date startingTime) {
		this.startingTime = startingTime;
	}
}
