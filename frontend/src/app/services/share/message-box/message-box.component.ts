import { Component, Input, OnInit } from '@angular/core';
import { MessageDTO } from './MessageDTO';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit {

  @Input() message: Observable<MessageDTO>;

  constructor() {
  }

  ngOnInit(): void {
  }

}
