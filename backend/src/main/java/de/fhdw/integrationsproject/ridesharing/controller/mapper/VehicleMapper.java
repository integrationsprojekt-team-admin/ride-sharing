package de.fhdw.integrationsproject.ridesharing.controller.mapper;

import de.fhdw.integrationsproject.ridesharing.controller.dto.builder.VehicleDTOBuilder;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Vehicle;
import de.fhdw.integrationsproject.ridesharing.controller.dto.VehicleDTO;
import de.fhdw.integrationsproject.ridesharing.model.builder.VehicleBuilder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class VehicleMapper {

    public VehicleDTO toVehicleDTO(Vehicle vehicle) {
        VehicleDTOBuilder vehicleDTOBuilder = new VehicleDTOBuilder()
                .setId(vehicle.getId().toString())
                .setVin(vehicle.getVin())
                .setLicensePlate(vehicle.getLicensePlate())
                .setRegistrationDate(vehicle.getRegistrationDate())
                .setPurchasePrice(vehicle.getPurchasePrice())
                .setBrand(vehicle.getBrand())
                .setModel(vehicle.getModel());

        if (vehicle.getLocations() != null) {
            vehicleDTOBuilder
                    .setLat(vehicle.getLocations().getLat())
                    .setLng(vehicle.getLocations().getLng());
        }

        return vehicleDTOBuilder.build();
    }

    public Vehicle toVehicle(VehicleDTO vehicleDTO) {
        VehicleBuilder vehicleBuilder = new VehicleBuilder();

        if (vehicleDTO.getId() != null) {
            vehicleBuilder.setId(UUID.fromString(vehicleDTO.getId()));
        }

        vehicleBuilder.setVin(vehicleDTO.getVin())
                .setLicensePlate(vehicleDTO.getLicensePlate())
                .setRegistrationDate(vehicleDTO.getRegistrationDate())
                .setPurchasePrice(vehicleDTO.getPurchasePrice())
                .setBrand(vehicleDTO.getBrand())
                .setModel(vehicleDTO.getModel())
                .setLocations(new Location(vehicleDTO.getLat(), vehicleDTO.getLng()));

        return vehicleBuilder.build();
    }
}