import { Component, OnInit } from '@angular/core';
import { VehicleService } from '../../services/vehicle.service';
import { Observable } from 'rxjs';
import { VehicleDTO } from '../../services/transport/VehicleDTO';
import { MatDialog } from '@angular/material/dialog';
import { VehicleEditComponent } from './vehicle-edit/vehicle-edit.component';
import { VehicleDeleteComponent } from './vehicle-delete/vehicle-delete.component';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  allVehicles$: Observable<VehicleDTO[]>;

  vehicles: VehicleDTO[] = [{
    id: 'ee15afde-2d6e-484a-9ddf-6db6aba3591a',
    vin: 'JH4KA2540GC007745',
    licensePlate: 'H-AB 1234',
    registrationDate: new Date('2018-08-17T15:00:00'),
    purchasePrice: 14000,
    brand: 'VW',
    model: 'Transporter 6.1 Kombi',
    lat: 52.376550,
    lng: 9.741046
  }];

  displayedColumns: string[] = ['id', 'vin', 'licensePlate', 'registrationDate', 'purchasePrice', 'brand', 'model', 'lat', 'lng', 'delete'];

  constructor(
    private vehicleService: VehicleService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.allVehicles$ = this.vehicleService.getAll();
    // this.allVehicles$ = of(this.vehicles);
  }

  new(): void {
    this.dialog.open(VehicleEditComponent, {
      data: new VehicleDTO()
    });
  }

  edit(element): void {
    this.dialog.open(VehicleEditComponent, {
      data: element,
    });
  }

  delete(element): void {
    this.dialog.open(VehicleDeleteComponent, {
      data: element
    });
  }
}
