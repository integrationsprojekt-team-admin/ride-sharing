package de.fhdw.integrationsproject.ridesharing.model.offerState;

import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferIllegalStateException;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Ride;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class RideOfferStateMachineTest {
	
	private static RideOffer offer;
	
	@BeforeEach
	void setUp() {
		Location start = new Location(52.36619938342916, 9.77279820758798);
		Location destination = new Location(52.37640931562144, 9.740794773412025);
		Ride ride = new Ride(start, destination);
		RideRequest rideRequest = new RideRequest(ride, LocalDateTime.now(), 1);
		offer = new RideOffer(ride, LocalDateTime.now(), 42d, rideRequest);
	}

	@Test
	void OfferedAfterConstruction() {
		assertEquals(RideOfferState.RideOfferStateOffered, offer.getState());
	}
	
	@Test
	void Offered_accept() {
		try {
			offer.accept();
			assertEquals(RideOfferState.RideOfferStateAccepted, offer.getState());
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
	}
	
	@Test
	void Offered_decline() {
		try {
			offer.decline();
			assertEquals(RideOfferState.RideOfferStateDeclined, offer.getState());
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
		
	}
	
	@Test
	void Accepted_accept() {
		try {
			offer.accept();
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
		assertThrows(RideOfferIllegalStateException.class, () -> offer.accept());
	}
	
	@Test
	void Accepted_decline() {
		try {
			offer.accept();
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
		assertThrows(RideOfferIllegalStateException.class, () -> offer.decline());
	}
	
	@Test
	void Declined_accept() {
		try {
			offer.decline();
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
		assertThrows(RideOfferIllegalStateException.class, () -> offer.accept());
	}
	
	@Test
	void Declined_decline() {
		try {
			offer.decline();
		} catch (RideOfferIllegalStateException e) {
			fail();
		}
		assertThrows(RideOfferIllegalStateException.class, () -> offer.decline());
	}

}
