package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.model.Vehicle;
import de.fhdw.integrationsproject.ridesharing.repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class VehicleService {

    /**
     * Injected vehicle repository fo database access.
     **/
    @Autowired
    private VehicleRepository vehicleRepository;

    /**
     * Database operation to retrieve all vehicle entities.
     * @return a list of vehicles.
     */
    public List<Vehicle> getAll() {
        return vehicleRepository.findAll();
    }

    /**
     * Database operation to get a specific vehicle entity by id.
     * @param id to identify the vehicle entity.
     * @return a concrete vehicle object
     */
    public Optional<Vehicle> getById(UUID id) {
        return this.vehicleRepository.findById(id);
    }

    /**
     * Database operation to persist a vehicle object.
     * @param vehicle which should be saved.
     * @return the new generated uuid of the saved vehicle object.
     */
    public UUID create(Vehicle vehicle) {
        return vehicleRepository.save(vehicle).getId();
    }

    /**
     * Database operation to delete a specific vehicle entity.
     * @param id to identify the vehicle entity.
     */
    public void delete(UUID id) {
        vehicleRepository.deleteById(id);
    }

    /**
     * Helper Operation to delete all vehicle entities.
     * Can be used as tearDown method in test scenarios.
     */
    public void deleteAll() {
        vehicleRepository.deleteAll();
    }
}
