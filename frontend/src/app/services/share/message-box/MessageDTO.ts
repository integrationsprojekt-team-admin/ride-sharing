export class MessageDTO {
  message: string;
  error: boolean;

  constructor(message: string, error = false) {
    this.message = message;
    this.error = error;
  }
}
