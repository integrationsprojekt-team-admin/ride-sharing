export class JwtResponse {
  id: string;
  accessToken: string;
  type: string;
  username: string;
  email: string;
  roles: Set<string>;
}
