package de.fhdw.integrationsproject.ridesharing.controller;

import de.fhdw.integrationsproject.ridesharing.controller.dto.request.LoginDTO;
import de.fhdw.integrationsproject.ridesharing.controller.dto.request.SignupDTO;
import de.fhdw.integrationsproject.ridesharing.controller.dto.response.JwtResponse;
import de.fhdw.integrationsproject.ridesharing.controller.dto.response.MessageResponse;
import de.fhdw.integrationsproject.ridesharing.exceptions.EmailAdressAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UsernameAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.AdminRole;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import de.fhdw.integrationsproject.ridesharing.security.jwt.JwtUtils;
import de.fhdw.integrationsproject.ridesharing.security.services.UserDetailsImpl;
import de.fhdw.integrationsproject.ridesharing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping(path = "/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		Set<String> roles = userDetails.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());

		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles));
	}

	@PostMapping(path = "/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupDTO signUpDTO) {
		User user = new User(signUpDTO.getUsername(),
				encoder.encode(signUpDTO.getPassword()),
				signUpDTO.getEmail());

		Set<String> strRoles = signUpDTO.getRoles();

		if (strRoles == null) {
			user.addRole(new CustomerRole());
		} else {
			for (String role : strRoles) {
				if (role.equals("admin")) {
					user.addRole(new AdminRole());
				} else {
					user.addRole(new CustomerRole());
				}
			}
		}

		try {
			this.userService.createUser(user);
		} catch (UsernameAlreadyExistsException e) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		} catch (EmailAdressAlreadyExistsException e) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
