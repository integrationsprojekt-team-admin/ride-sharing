import { browser, logging } from 'protractor';
import { VehiclePage } from './vehicle.po';
import { AuthPage } from './auth.po';

describe('VehiclePage', () => {
  let page: VehiclePage;

  beforeEach(async () => {
    page = new VehiclePage();
    const auth = new AuthPage();
    try {
      await auth.loginAsAdmin();
    } catch (e) {
    }
  });

  it('should display vehicle', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('Fahrzeuge');
  });

  it('should add vehicle', async () => {
    await page.navigateTo();
    await page.getAddButton().click();
    expect(await page.getDialogTitleText()).toEqual('Fahrzeug erstellen');
    await page.fillVehicleForm();
    await page.getCreateButton().click();
    expect(await page.getLicensePlateCell()).toEqual('H-AB 1234');
  });

  /*
  it('should update vehicle', async () => {
    await page.navigateTo();
    expect(await page.getLicensePlateCell()).toEqual('H-AB 1234');
    page.getEditButton().click();
    await browser.sleep(2000);
    expect(await page.getDialogTitleText()).toEqual('Fahrzeug bearbeiten');
    page.fillVehicleUpdateForm();
    await browser.sleep(2000);
    page.getConfirmEditButton().click();
    await browser.sleep(2000);
    expect(await page.getLicensePlateCell()).toEqual('B-XY 9876');
  });
  */

  it('should delete vehicle', async () => {
    await page.navigateTo();
    await page.getDeleteButton().click();
    await page.getConfirmDeleteButton().click();
    try {
      await page.getLicensePlateCell();
      fail('The vehicle was not deleted.');
    } catch (NoSuchElementError) {
      // The vehicle has been deleted successfully.
    }
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
