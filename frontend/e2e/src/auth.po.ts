import { browser, by, element } from 'protractor';

export class AuthPage {

  async navigateTo(): Promise<any> {
    return browser.get(browser.baseUrl + 'login');
  }

  async loginAsAdmin(): Promise<any> {
    await this.navigateTo();
    await this.fillLoginFormAsAdmin();
    return this.submitLogin();
  }

  async loginAsInvalidUser(): Promise<any> {
    await this.navigateTo();
    await this.fillLoginFormAsInvalidUser();
    return this.submitLogin();
  }

  async fillLoginFormAsAdmin(): Promise<void> {
    await element(by.id('username')).sendKeys('admin');
    return element(by.id('password')).sendKeys('admin');
  }

  async fillLoginFormAsInvalidUser(): Promise<void> {
    await element(by.id('username')).sendKeys('InvalidUser');
    return element(by.id('password')).sendKeys('InvalidUser');
  }

  async submitLogin(): Promise<void> {
    return element(by.id('login')).click();
  }

  async navigateToRegister(): Promise<void> {
    return element(by.css('#register-link a')).click();
  }

  async fillRegisterForm(): Promise<void> {
    await element(by.id('username')).sendKeys('Max Mustermann');
    await element(by.id('email')).sendKeys('Max.Mustermann@fhdw.de');
    return element(by.id('password')).sendKeys('123456');
  }

  async submitRegister(): Promise<void> {
    return element(by.id('register')).click();
  }
}
