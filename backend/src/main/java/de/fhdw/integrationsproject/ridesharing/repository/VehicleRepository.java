package de.fhdw.integrationsproject.ridesharing.repository;

import de.fhdw.integrationsproject.ridesharing.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Own JPA repositoriy espacially for vehicles to realize database access.
 */
public interface VehicleRepository extends JpaRepository<Vehicle, UUID> {
}
