package de.fhdw.integrationsproject.ridesharing.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fhdw.integrationsproject.ridesharing.controller.dto.VehicleDTO;
import de.fhdw.integrationsproject.ridesharing.controller.dto.builder.VehicleDTOBuilder;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Vehicle;
import de.fhdw.integrationsproject.ridesharing.service.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@Transactional
public class VehicleControllerTest {

	private final ObjectMapper mapper = new ObjectMapper();
	private MockMvc mvc;
	@InjectMocks
	private VehicleController vehicleController;
	@MockBean
	private VehicleService vehicleService;
	@Autowired
	private WebApplicationContext context;

	@BeforeEach
	public void init() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"AdminRole"})
	public void testGetById() throws Exception {
		Location location = new Location(9.741046, 52.376550);
		Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse("1995-12-17T03:24:00");
		UUID uuid = UUID.randomUUID();
		Vehicle vehicle = new Vehicle(uuid, location, 4, 0, "JH4KA2540GC007745", "H-AB 1234", date, 14000, "VW", "Transporter 6.1 Kombi");

		when(vehicleService.getById(ArgumentMatchers.any())).thenReturn(Optional.of(vehicle));
		mvc.perform(MockMvcRequestBuilders
				.get("/vehicles/" + uuid.toString()))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"AdminRole"})
	public void testCreate() throws Exception {

		VehicleDTO vehicleDTO = new VehicleDTOBuilder()
				.setVin("JH4KA2540GC007745")
				.setLicensePlate("H-AB 1234")
				.setRegistrationDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse("1995-12-17T03:24:00"))
				.setPurchasePrice(14000)
				.setBrand("VW")
				.setModel("Transporter 6.1 Kombi")
				.setLat(52.376550)
				.setLng(9.741046).build();

		UUID uuid = UUID.randomUUID();
		String json = mapper.writeValueAsString(vehicleDTO);

		when(vehicleService.create(ArgumentMatchers.any())).thenReturn(uuid);
		mvc.perform(MockMvcRequestBuilders
				.post("/vehicles")
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf-8")
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"AdminRole"})
	public void testGetAll() throws Exception {

		Location location = new Location(9.741046,52.376550);
        Date date =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse("1995-12-17T03:24:00");
        UUID uuid1 = UUID.randomUUID();
        UUID uuid2 = UUID.randomUUID();

		Vehicle vehicle1 = new Vehicle(uuid1,location,4,0,"JH4KA2540GC007745","H-AB 1234",date,14000,"VW","Transporter 6.1 Kombi");
        Vehicle vehicle2 = new Vehicle(uuid2,location,3,0,"1G1JC1243T7246823","H-XY 6789",date,22000,"Mercedes","Sprinter");
        List<Vehicle> vehicles = Arrays.asList(vehicle1, vehicle2);

		when(vehicleService.getAll()).thenReturn(vehicles);
		mvc.perform(MockMvcRequestBuilders
				.get("/vehicles"))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = "admin", authorities = {"AdminRole"})
	public void testDelete() throws Exception {

		UUID uuid = UUID.randomUUID();
//		when(vehicleService.delete(ArgumentMatchers.any())).thenReturn(null);
		mvc.perform(MockMvcRequestBuilders
				.delete("/vehicles/" + uuid))
				.andExpect(status().isNoContent());
	}


}
