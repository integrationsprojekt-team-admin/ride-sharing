package de.fhdw.integrationsproject.ridesharing.controller.dto.builder;

import de.fhdw.integrationsproject.ridesharing.controller.dto.VehicleDTO;

import java.util.Date;

public class VehicleDTOBuilder {

    private String id;
    private String vin;
    private String licensePlate;
    private Date registrationDate;
    private double purchasePrice;
    private String brand;
    private String model;

    // Corresponds to Location class
    private double lat;
    private double lng;

    public VehicleDTOBuilder setId(String id) {
        this.id = id;
        return this;
    }

    public VehicleDTOBuilder setVin(String vin) {
        this.vin = vin;
        return this;
    }

    public VehicleDTOBuilder setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
        return this;
    }

    public VehicleDTOBuilder setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public VehicleDTOBuilder setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
        return this;
    }

    public VehicleDTOBuilder setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public VehicleDTOBuilder setModel(String model) {
        this.model = model;
        return this;
    }

    public VehicleDTOBuilder setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public VehicleDTOBuilder setLng(double lng) {
        this.lng = lng;
        return this;
    }

    public VehicleDTO build() {
        return new VehicleDTO(id, vin, licensePlate, registrationDate, purchasePrice, brand, model, lat, lng);
    }
}