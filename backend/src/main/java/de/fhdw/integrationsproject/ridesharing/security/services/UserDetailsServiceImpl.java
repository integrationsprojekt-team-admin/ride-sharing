package de.fhdw.integrationsproject.ridesharing.security.services;

import de.fhdw.integrationsproject.ridesharing.exceptions.UserNotInContextException;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
		return UserDetailsImpl.build(user);
	}

	private UserDetails getUserDetailsFromContext() throws UserNotInContextException {
		if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetailsImpl)
			return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		throw new UserNotInContextException("User is not in Context.");
	}

	@Transactional
	public User getUserFromContext() throws UserNotInContextException {
		Optional<User> optionalUser = this.userRepository.findByUsername(this.getUserDetailsFromContext().getUsername());
		if (optionalUser.isEmpty()) throw new UserNotInContextException("User is not in Context.");
		return optionalUser.get();
	}
}
