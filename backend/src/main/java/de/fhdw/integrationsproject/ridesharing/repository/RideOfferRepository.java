package de.fhdw.integrationsproject.ridesharing.repository;


import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RideOfferRepository extends JpaRepository<RideOffer, UUID> {

	List<RideOffer> findByCustomer(CustomerRole customerRole);
}
