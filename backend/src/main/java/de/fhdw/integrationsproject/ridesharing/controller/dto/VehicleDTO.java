package de.fhdw.integrationsproject.ridesharing.controller.dto;
import java.util.Date;

public class VehicleDTO {

    private String id;
    private String vin;
    private String licensePlate;
    private Date registrationDate;
    private double purchasePrice;
    private String brand;
    private String model;

    // Corresponds to Location class
    private double lat;
    private double lng;

    public VehicleDTO(String id, String vin, String licensePlate, Date registrationDate, double purchasePrice, String brand, String model, double lat, double lng) {
        this.id = id;
        this.vin = vin;
        this.licensePlate = licensePlate;
        this.registrationDate = registrationDate;
        this.purchasePrice = purchasePrice;
        this.brand = brand;
        this.model = model;
        this.lat = lat;
        this.lng = lng;
    }

    public String getId() {
        return id;
    }

    public String getVin() {
        return vin;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}