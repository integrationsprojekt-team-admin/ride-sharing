package de.fhdw.integrationsproject.ridesharing.exceptions;

public abstract class RideSharingException extends Exception {

	public RideSharingException() {
	}

	public RideSharingException(String message) {
		super(message);
	}

	public RideSharingException(String message, Throwable cause) {
		super(message, cause);
	}

	public RideSharingException(Throwable cause) {
		super(cause);
	}
}
