package de.fhdw.integrationsproject.ridesharing.model.user;

import de.fhdw.integrationsproject.ridesharing.model.user.role.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


/**
 * represents a user.
 * must have an unique email-address and username.
 */
@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue
	private UUID id;

	@NotEmpty
	@Column(unique = true)
	private String username;

	@NotEmpty
	private String password;

	@Email
	@Column(unique = true)
	private String email;

	@OneToMany(cascade = CascadeType.ALL)
	private Set<Role> roles;

	public User() {
	}

	public User(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}

	public UUID getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void addRoles(Set<Role> roles) {
		if (this.roles == null) this.roles = new HashSet<>();
		this.roles.addAll(roles);
	}

	public void addRole(Role role) {
		if (this.roles == null) this.roles = new HashSet<>();
		this.roles.add(role);
	}
}
