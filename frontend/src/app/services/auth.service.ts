import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { LoginDTO } from './transport/LoginDTO';
import { SignupDTO } from './transport/SignupDTO';
import { JwtResponse } from './transport/JwtResponse';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(login: LoginDTO): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(environment.baseUrl + 'auth/signin', login, httpOptions);
  }

  register(signup: SignupDTO): Observable<any> {
    return this.http.post(environment.baseUrl + 'auth/signup', signup, httpOptions);
  }
}
