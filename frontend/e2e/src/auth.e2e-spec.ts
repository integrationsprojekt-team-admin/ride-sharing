import { AuthPage } from './auth.po';
import { browser, by, element, logging } from 'protractor';

describe('AuthPage', () => {
  let page: AuthPage;

  beforeEach(() => {
    page = new AuthPage();
    //browser.executeScript('window.sessionStorage.clear();');
  });

  it('should register a user', async () => {
    await page.navigateTo();
    await page.navigateToRegister();
    await page.fillRegisterForm();
    await page.submitRegister();
    expect(await element.all(by.css('mat-error')).count()).toBe(0, 'No errors should appear');
  });

  it('should not login', async () => {
    await page.loginAsInvalidUser();
    expect(await browser.getCurrentUrl()).toBe(browser.baseUrl + 'login');
  });

  it('should login', async () => {
    await page.loginAsAdmin();
    expect(await browser.getCurrentUrl()).toBe(browser.baseUrl + 'admin-view');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.ALL,
    } as logging.Entry));
  });
});
