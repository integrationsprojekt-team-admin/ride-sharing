package de.fhdw.integrationsproject.ridesharing.model;

import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferIllegalStateException;
import de.fhdw.integrationsproject.ridesharing.model.offerState.RideOfferState;

import javax.persistence.*;
import java.time.LocalDateTime;


/**
 * A ride offered to a customer as a reply to a request.
 */
@Entity
@Table(name = "ride_offer")
public class RideOffer extends ScheduledRide {

	/**
	 * The price to be paid by the customer for the offered ride.
	 */
	private Double price;

	/**
	 * The original request, which is answered with this offer.
	 */
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "REQUEST_ID", foreignKey = @ForeignKey(name = "REQUEST_ID_FK"))
	private RideRequest request;

	@ManyToOne
	@JoinColumn(name = "VEHICLE_ID", foreignKey = @ForeignKey(name = "VEHICLE_ID_FK"))
	private Vehicle vehicle;

	/**
	 * This offer's state.
	 */
	@Enumerated(EnumType.STRING)
	private RideOfferState state;

	public RideOffer(Ride ride, LocalDateTime startingTime, Double price, RideRequest request) {
		super(ride, startingTime);
		this.price = price;
		this.request = request;
		this.state = RideOfferState.RideOfferStateOffered;
	}

	public RideRequest getRequest() {
		return request;
	}

	public RideOffer() {
	}

	/**
	 * Accept this offer.
	 *
	 * @throws RideOfferIllegalStateException
	 */
	public void accept() throws RideOfferIllegalStateException {
		this.state = this.state.accept();
	}

	/**
	 * Decline this offer.
	 *
	 * @throws RideOfferIllegalStateException
	 */
	public void decline() throws RideOfferIllegalStateException {
		this.state = this.state.decline();
	}

	public Double getPrice() {
		return price;
	}


	@Override
	public String toString() {
		return "Angebot: " + super.toString() + " Preis: " + this.price.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RideOffer)) {
			return false;
		}
		RideOffer other = (RideOffer) obj;
		return (this.getRide().equals(other.getRide())
				&& this.getStartingTime().equals(other.getStartingTime())
				&& this.price.equals(other.price)
				&& this.request.equals(other.request));
	}

	public RideOfferState getState() {
		return this.state;
	}

}
