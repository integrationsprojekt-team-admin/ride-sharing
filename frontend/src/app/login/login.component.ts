import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { TokenStorageService } from '../services/token-storage.service';
import { LoginDTO } from '../services/transport/LoginDTO';
import { BehaviorSubject } from 'rxjs';
import { MessageDTO } from '../services/share/message-box/MessageDTO';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  registerForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  signinInvalid = false;
  load = false;
  hide = true;
  message = new BehaviorSubject<MessageDTO>(new MessageDTO(''));

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private tokenStorage: TokenStorageService) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.load = true;
    const {username, password} = this.registerForm.getRawValue();
    this.authService.login(new LoginDTO(username, password)).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data.username);
        this.tokenStorage.saveRole(data.roles);
        this.reloadPage();
      },
      err => {
        this.signinInvalid = true;
        this.load = false;
        this.message.next(new MessageDTO('Die Kombination aus Benutzernamen und Passwort stimmt nicht überein.', true));
      },
      () => {
        this.load = false;
      }
    );
  }

  reloadPage(): void {
    window.location.reload();
  }
}
