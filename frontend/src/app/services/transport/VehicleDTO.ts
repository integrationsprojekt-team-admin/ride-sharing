import { LocationDTO } from './LocationDTO';

export class VehicleDTO {
  id: string;
  vin: string;
  licensePlate: string;
  registrationDate: Date;
  purchasePrice: number;
  brand: string;
  model: string;
  // location: LocationDTO;
  lat: number;
  lng: number;
}
