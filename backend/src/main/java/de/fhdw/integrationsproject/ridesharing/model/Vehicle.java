package de.fhdw.integrationsproject.ridesharing.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * Represents a vehicle for transportation of customer.
 */
@Entity
@Table(name = "Vehicle")
public class Vehicle {

    /**
     * This is the specified identifier for the entity.
     */
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * The current location of this instance.
     */
    private Location locations;

    /**
     * The maximum number of seats.
     */
    private int maxSeats;

    /**
     * The number of seats currently taken.
     */
    private int takenSeats;
    //private VehicleState vehicleState;

    /**
     * The vehicle identifictation number (vin) which is unique.
     */
    @Column(unique = true)
    private String vin;

    /**
     * The vehicle's license plate of this instance.
     */
    private String licensePlate;

    /**
     * The date of the first registration.
     */
    private Date registrationDate;

    /**
     * The vehicle's purchase price (represented as internal credits).
     */
    private double purchasePrice;

    /**
     * The vehicle's brand.
     */
    private String brand;

    /**
     * The vehicle's model type.
     */
    private String model;

    public Vehicle(UUID id, Location locations, int maxSeats, int takenSeats, String vin, String licensePlate, Date registrationDate, double purchasePrice, String brand, String model) {
        this.id = id;
        this.locations = locations;
        this.maxSeats = maxSeats;
        this.takenSeats = takenSeats;
        this.vin = vin;
        this.licensePlate = licensePlate;
        this.registrationDate = registrationDate;
        this.purchasePrice = purchasePrice;
        this.brand = brand;
        this.model = model;
    }

    public Vehicle() {
    }

    public UUID getId() {
        return id;
    }

    public Location getLocations() {
        return locations;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public int getTakenSeats() {
        return takenSeats;
    }

    public String getVin() {
        return vin;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return maxSeats == vehicle.maxSeats
                && takenSeats == vehicle.takenSeats
                && Double.compare(vehicle.purchasePrice, purchasePrice) == 0
                && Objects.equals(id, vehicle.id)
                && Objects.equals(locations, vehicle.locations)
                && Objects.equals(vin, vehicle.vin)
                && Objects.equals(licensePlate, vehicle.licensePlate)
                && Objects.equals(registrationDate, vehicle.registrationDate)
                && Objects.equals(brand, vehicle.brand)
                && Objects.equals(model, vehicle.model);
    }

    /**
     * Equals especially for vehicle.
     *
     * @param o the compare object
     * @return true or false if the objects subject specifically match
     */
    public boolean equalsSubjectSpecific(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;
        return this.vin.equals(vehicle.getVin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, locations, maxSeats, takenSeats, vin, licensePlate, registrationDate, purchasePrice, brand, model);
    }
}
