package de.fhdw.integrationsproject.ridesharing.model.user.role;

import de.fhdw.integrationsproject.ridesharing.visitor.RoleVisitor;

import javax.persistence.Entity;

/**
 * represents
 */
@Entity
public class AdminRole extends Role {
	@Override
	public void accept(RoleVisitor roleVisitor) {
		roleVisitor.handleAdminRole(this);
	}

}
