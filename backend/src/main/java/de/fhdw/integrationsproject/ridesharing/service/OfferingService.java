package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferIllegalStateException;
import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferNotFoundException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UserHasNoPermissionException;
import de.fhdw.integrationsproject.ridesharing.model.RideOffer;
import de.fhdw.integrationsproject.ridesharing.model.RideRequest;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import de.fhdw.integrationsproject.ridesharing.repository.RideOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class OfferingService {

	@Autowired
	private RideOfferRepository rideOfferRepository;

	@Autowired
	private UserService userService;

	/**
	 * accept request and return possible offers
	 *
	 * @param rideRequest requestinformation for caluclating
	 * @param user
	 * @return list of possible offers
	 */
	@Transactional
	public List<RideOffer> acceptRequest(RideRequest rideRequest, User user) {
		this.addRideRequest(rideRequest, user);
		List<RideOffer> offers = calculateOffers(rideRequest);
		this.addRideOffers(offers, user);
		return this.rideOfferRepository.saveAll(offers);
	}

	@Transactional
	void addRideOffers(List<RideOffer> offers, User user) {
		Optional<CustomerRole> customerRole = user.getRoles().stream()
				.filter(role -> role instanceof CustomerRole)
				.map(role -> (CustomerRole) role)
				.findFirst();

		if (customerRole.isPresent()) {
			CustomerRole role = customerRole.get();
			offers.forEach(rideOffer -> rideOffer.setCustomer(role));
			role.addOffers(offers);
		}
	}

	@Transactional
	void addRideRequest(RideRequest rideRequest, User user) {
		Optional<CustomerRole> customerRole = user.getRoles().stream()
				.filter(role -> role instanceof CustomerRole)
				.map(role -> (CustomerRole) role)
				.findFirst();

		if (customerRole.isPresent()) {
			CustomerRole role = customerRole.get();
			rideRequest.setCustomer(role);
			role.addRequest(rideRequest);
		}
	}

	private List<RideOffer> calculateOffers(RideRequest rideRequest) {
		List<RideOffer> offers = Collections.singletonList(new RideOffer(rideRequest.getRide(), rideRequest.getStartingTime(), 1d, rideRequest));
		//TODO calculate multiple offers based on available vehicles, following is only for demo
		return offers;
	}

	public RideOffer getById(UUID id) throws RideOfferNotFoundException {
		Optional<RideOffer> optionalRideOffer = this.rideOfferRepository.findById(id);
		if (optionalRideOffer.isEmpty())
			throw new RideOfferNotFoundException("RideOffer with ID " + id.toString() + " not found!");
		return optionalRideOffer.get();
	}

	@Transactional
	public RideOffer answerOffer(RideOffer offer, boolean accepted, User user) throws RideOfferIllegalStateException, UserHasNoPermissionException {
		if (!user.getRoles().contains(offer.getCustomer()))
			throw new UserHasNoPermissionException("User " + user.getUsername() + " has no Permission to access offer with id " + offer.getId().toString());
		if (accepted) {
			offer.accept();
		} else {
			offer.decline();
		}
		return this.rideOfferRepository.save(offer);
	}
}
