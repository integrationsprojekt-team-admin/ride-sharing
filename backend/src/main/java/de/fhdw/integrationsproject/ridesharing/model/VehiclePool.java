package de.fhdw.integrationsproject.ridesharing.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Represents a Vehicle-Pool to group vehicles of a certain type.
 */
@Entity
@Table(name = "VehiclePool")
public class VehiclePool {

    /**
     * This is the specified identifier for the entity.
     */
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * The list of vehicles of the same type ???
     */
    @OneToMany
    private List<Vehicle> vehicles;

    public VehiclePool() {
    }

    /**
     * Adds a specified vehicle.
     * @param vehicle to be added.
     */
    public void addVehicle(Vehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    /**
     * Removes a specified vehicle.
     * @param vehicle to be removed.
     */
    public void removeVehicle(Vehicle vehicle) {
        this.vehicles.remove(vehicle);
    }

}
