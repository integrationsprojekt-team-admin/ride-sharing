package de.fhdw.integrationsproject.ridesharing.model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * Represents a headquarter to control arbitrary vehicle-pools.
 */
@Entity
@Table(name = "Headquarter")
public class Headquarter {

    /**
     * This is the specified identifier for the entity.
     */
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    /**
     * List of arbitrary vehicle-pools to differ between vehicle-types.
     */
    @OneToMany
    private List<VehiclePool> vehiclePools;

    /**
     * Serves the vehicle as a central.
     */
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "lat", column = @Column(name = "location_lat")),
            @AttributeOverride(name = "lng", column = @Column(name = "location_lng")),
    })
    private Location location;

    /**
     * The headquartes name for better identification.
     */
    private String name;

    public Headquarter() {
    }
}
