package de.fhdw.integrationsproject.ridesharing.exceptions;

public class RideOfferNotFoundException extends RideSharingException {
	public RideOfferNotFoundException(String message) {
		super(message);
	}
}
