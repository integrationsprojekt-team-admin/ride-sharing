package de.fhdw.integrationsproject.ridesharing.exceptions;

public class UserNotFoundException extends RideSharingException {
	public UserNotFoundException(String s) {
		super(s);
	}
}
