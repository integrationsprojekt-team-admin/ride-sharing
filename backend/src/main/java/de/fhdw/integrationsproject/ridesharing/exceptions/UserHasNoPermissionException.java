package de.fhdw.integrationsproject.ridesharing.exceptions;


public class UserHasNoPermissionException extends RideSharingException {
	public UserHasNoPermissionException(String s) {
		super(s);
	}
}
