package de.fhdw.integrationsproject.ridesharing.config;

import de.fhdw.integrationsproject.ridesharing.exceptions.EmailAdressAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.exceptions.UsernameAlreadyExistsException;
import de.fhdw.integrationsproject.ridesharing.model.user.User;
import de.fhdw.integrationsproject.ridesharing.model.user.role.AdminRole;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;
import de.fhdw.integrationsproject.ridesharing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DefaultInstanceRunner implements CommandLineRunner {

	@Autowired
	private UserService userService;

	@Override
	public void run(String... args) throws Exception {
		createAdminUser();
		createCustomerUser();
	}

	@Transactional
	void createAdminUser() {
		User admin = new User();
		admin.setUsername("admin");
		this.userService.encodeAndSetPassword("admin", admin);
		admin.setEmail("admin@ride-sharing.de");
		admin.addRole(new AdminRole());
		try {
			this.userService.createUser(admin);
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException ignored) {
		}
	}

	@Transactional
	void createCustomerUser() {
		User customer = new User();
		customer.setUsername("customer");
		this.userService.encodeAndSetPassword("customer", customer);
		customer.setEmail("customer@ride-sharing.de");
		customer.addRole(new CustomerRole());
		try {
			this.userService.createUser(customer);
		} catch (UsernameAlreadyExistsException | EmailAdressAlreadyExistsException ignored) {
		}
	}
}
