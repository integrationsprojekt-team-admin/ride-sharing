package de.fhdw.integrationsproject.ridesharing.service;

import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Vehicle;
import de.fhdw.integrationsproject.ridesharing.model.builder.VehicleBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Transactional
public class VehicleServiceTest {

    @Autowired
    VehicleService vehicleService;
    private Location location1;
    private Vehicle vehicle1;
    private Location location2;
    private Vehicle vehicle2;

    @BeforeEach
    void setUp() {
        location1 = new Location(52.376550, 9.741046);
        vehicle1 = new VehicleBuilder()
                .setVin("JH4KA2540GC007745")
                .setLicensePlate("H-AB 1234")
                .setRegistrationDate(Date.valueOf("1995-12-17"))
                .setPurchasePrice(14000)
                .setBrand("VW")
                .setModel("Transporter 6.1 Kombi")
                .setLocations(location1)
                .build();
        location2 = new Location(52.366200, 9.772470);
        vehicle2 = new VehicleBuilder()
                .setVin("NM0AE8F79F1183066")
                .setLicensePlate("B-CD 5042")
                .setRegistrationDate(Date.valueOf("2010-02-17"))
                .setPurchasePrice(25000)
                .setBrand("Mercedes")
                .setModel("250 Marco Polo")
                .setLocations(location2)
                .build();
    }

    @AfterEach
    void tearDown() {
        vehicleService.deleteAll();
    }

    @Test
    void testGetById() {
        assertEquals(Optional.empty(), vehicleService.getById(UUID.randomUUID()));
        var uuid = vehicleService.create(vehicle1);
        assertEquals(vehicle1, vehicleService.getById(uuid).get());
    }

    @Test
    void testCreateById() {
        var uuid = vehicleService.create(vehicle1);
        assertEquals(Optional.of(vehicle1), vehicleService.getById(uuid));
    }

    @Test
    void testCreateGetAll() {
        vehicleService.create(vehicle1);
        vehicleService.create(vehicle2);
        assertEquals(Arrays.asList(vehicle1, vehicle2), vehicleService.getAll());
    }

    @Test
    void testDelete() {
        var uuid = vehicleService.create(vehicle1);
        vehicleService.create(vehicle2);
        assertEquals(Arrays.asList(vehicle1, vehicle2), vehicleService.getAll());
        vehicleService.delete(uuid);
        assertEquals(Collections.singletonList(vehicle2), vehicleService.getAll());
    }
}
