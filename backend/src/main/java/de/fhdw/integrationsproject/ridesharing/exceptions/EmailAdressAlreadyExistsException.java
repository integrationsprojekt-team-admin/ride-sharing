package de.fhdw.integrationsproject.ridesharing.exceptions;

public class EmailAdressAlreadyExistsException extends RideSharingException {
	public EmailAdressAlreadyExistsException(String s) {
		super(s);
	}
}
