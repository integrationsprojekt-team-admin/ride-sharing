package de.fhdw.integrationsproject.ridesharing.exceptions;

public class UsernameAlreadyExistsException extends RideSharingException {
	public UsernameAlreadyExistsException(String s) {
		super(s);
	}
}
