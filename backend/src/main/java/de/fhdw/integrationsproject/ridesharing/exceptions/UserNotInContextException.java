package de.fhdw.integrationsproject.ridesharing.exceptions;


public class UserNotInContextException extends RuntimeException {
	public UserNotInContextException(String message) {
		super(message);
	}
}
