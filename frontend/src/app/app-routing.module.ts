import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminViewComponent } from './admin-view/admin-view.component';
import { CustomerViewComponent } from './customer-view/customer-view.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SessionValidGuard } from './session-valid.guard';
import { SessionInvalidGuard } from './session-invalid.guard';

const routes: Routes = [
  {
    component: AdminViewComponent,
    path: 'admin-view',
    canActivate: [SessionValidGuard]
  },
  {
    component: LoginComponent,
    path: 'login',
    canActivate: [SessionInvalidGuard]
  },
  {
    component: RegisterComponent,
    path: 'register',
    canActivate: [SessionInvalidGuard]
  },
    {
    component: CustomerViewComponent,
    path: 'customer-view'
  },
  {
    redirectTo: 'login',
    path: '**'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
