import { Injectable } from '@angular/core';
import { RideRequestDTO } from './transport/riderequestdto';
import { RideRequestApiService } from './ride-request-api.service';
import { Observable } from 'rxjs';
import {RideOfferDTO} from "./transport/RideOfferDTO";

@Injectable({
    providedIn: 'root'
})
export class RideRequestService {

    submitRequest(rideRequest: RideRequestDTO): Observable<RideOfferDTO[]> {
        return this.rideRequestApiService.submitRequest(rideRequest);
    }

    constructor(
        private rideRequestApiService: RideRequestApiService,
    ) { }

  acceptOffer(id: string, accepted: boolean): Observable<any> {
    return this.rideRequestApiService.acceptOffer(id, accepted);
  }
}
