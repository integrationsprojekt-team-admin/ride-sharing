package de.fhdw.integrationsproject.ridesharing.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * Marks a location in a simple plane by giving lattitude (lat.) and longitunde (lng).
 */
@Embeddable
public class Location {

	/**
	 * This location's longitude.
	 */
	private Double lng;

	/**
	 * This location's lattitude.
	 */
	private Double lat;

	public Location(Double longitude, Double lattitude) {
		this.lng = longitude;
		this.lat = lattitude;
	}

	public Location() {
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Location)) {
			return false;
		}
		Location other = (Location) obj;
		return (this.lng.equals(other.lng) && this.lat.equals(other.lat));
	}

	@Override
	public String toString() {
		return "Laenge: " + this.lng + " Breite: " + this.lat;
	}

	public Double getLng() {
		return lng;
	}

	public Double getLat() {
		return lat;
	}
}
