import { TestBed } from '@angular/core/testing';

import { RideRequestApiService } from './ride-request-api.service';

describe('RideRequestApiService', () => {
  let service: RideRequestApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RideRequestApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
