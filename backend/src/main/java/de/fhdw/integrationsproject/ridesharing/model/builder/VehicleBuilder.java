package de.fhdw.integrationsproject.ridesharing.model.builder;

import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.model.Vehicle;

import javax.persistence.OneToOne;
import java.util.Date;
import java.util.UUID;

public class VehicleBuilder {

    private UUID id;
    private Location locations;
    private int maxSeats;
    private int takenSeats;
    //private VehicleState vehicleState;
    private String vin;
    private String licensePlate;
    private Date registrationDate;
    private double purchasePrice;
    private String brand;
    private String model;

    public VehicleBuilder setId(UUID id) {
        this.id = id;
        return this;
    }

    public VehicleBuilder setLocations(Location locations) {
        this.locations = locations;
        return this;
    }

    public VehicleBuilder setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
        return this;
    }

    public VehicleBuilder setTakenSeats(int takenSeats) {
        this.takenSeats = takenSeats;
        return this;
    }

    public VehicleBuilder setVin(String vin) {
        this.vin = vin;
        return this;
    }

    public VehicleBuilder setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
        return this;
    }

    public VehicleBuilder setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
        return this;
    }

    public VehicleBuilder setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
        return this;
    }

    public VehicleBuilder setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public VehicleBuilder setModel(String model) {
        this.model = model;
        return this;
    }

    public Vehicle build() {
        return new Vehicle(id, locations, maxSeats, takenSeats, vin, licensePlate, registrationDate, purchasePrice, brand, model);
    }

}
