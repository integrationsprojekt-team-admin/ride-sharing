package de.fhdw.integrationsproject.ridesharing.visitor;

import de.fhdw.integrationsproject.ridesharing.model.user.role.AdminRole;
import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;

public interface RoleVisitor {

	void handleAdminRole(AdminRole adminRole);

	void handleCustomerRole(CustomerRole customerRole);
}
