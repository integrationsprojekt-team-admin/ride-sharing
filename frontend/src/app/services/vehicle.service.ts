import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { VehicleDTO } from './transport/VehicleDTO';
import { VehicleApiService } from './vehicle-api.service';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  private vehicles$ = new BehaviorSubject<VehicleDTO[]>([]);

  constructor(
    private vehicleApiService: VehicleApiService,
  ) {
    this.refreshReferences();
  }

  getAll(): Observable<VehicleDTO[]> {
    return this.vehicles$.asObservable();
  }

  editVehicle(vehicle: VehicleDTO): void {
    if (vehicle.id) {
      this.vehicleApiService.edit(vehicle).subscribe(() => this.refreshReferences());
    } else {
      this.vehicleApiService.create(vehicle).subscribe(() => this.refreshReferences());
    }
  }

  delete(vehicle: VehicleDTO): void {
    this.vehicleApiService.delete(vehicle).subscribe(() => this.refreshReferences());
  }

  private refreshReferences(): void {
    this.vehicleApiService.getAll().subscribe(vehicles => this.vehicles$.next(vehicles));
  }
}
