export class LocationDTO {
    lat: number;
    lng: number;
}