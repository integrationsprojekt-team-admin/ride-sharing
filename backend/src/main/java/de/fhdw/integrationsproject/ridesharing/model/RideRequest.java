package de.fhdw.integrationsproject.ridesharing.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import de.fhdw.integrationsproject.ridesharing.model.user.role.CustomerRole;

import java.time.LocalDateTime;

/**
 * Represents a request for a ride (by a customer).
 */
@Entity
@Table(name = "ride_request")
public class RideRequest extends ScheduledRide {


	@NotNull
	@Positive
	private int numberOfPassengers;

	public RideRequest(Ride ride, LocalDateTime startingTime, int numberOfPassengers) {
		super(ride, startingTime);
		this.numberOfPassengers = numberOfPassengers;
	}

	public RideRequest() {
	}

	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}

	@Override
	public String toString() {
		return "Anfrage: " + super.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(!(obj instanceof RideRequest)) {
			return false;
		}
		RideRequest other = (RideRequest) obj;
		return(this.getRide().equals(other.getRide())
				&& this.getStartingTime().equals(other.getStartingTime())
		);
	}

}
