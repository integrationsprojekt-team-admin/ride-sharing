import { Component, OnInit, Inject } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { RideRequestDTO } from 'src/app/services/transport/riderequestdto';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RideRequestService } from 'src/app/services/ride-request.service';
import {RideOfferDTO} from "../../../services/transport/RideOfferDTO";

@Component({
  selector: 'app-ride-request',
  templateUrl: './ride-request.component.html',
  styleUrls: ['./ride-request.component.css']
})
export class RideRequestComponent implements OnInit {
   formGroup: FormGroup;
   data: RideRequestDTO;
   currentOffers: RideOfferDTO[];


  constructor(
    private formBuilder: FormBuilder,
    private service: RideRequestService
  ) { }

  ngOnInit(): void {
    this.data = new RideRequestDTO();
    this.createForm();
    this.formGroup.get("startingTime").setValue(new Date());
  }
  createForm(): void {
    this.formGroup = this.formBuilder.group({
      start: [this.data.startingPlace],
      destination: [this.data.targetDestination],
      numberOfPersons: [this.data.numberOfPersons],
      startingTime: [this.data.startingTime]
    });
  }

  submitRequest(data: RideRequestDTO): void {
    console.log(data);
    this.service.submitRequest(data).subscribe(data=>this.currentOffers = data);
  }

  changeTime($event: Event): void {
    //this.time.setUTCHours(11:11 am)
    console.log($event);
    const selectedTime: string = (<HTMLInputElement>$event.target).value;
    if(selectedTime !== "") {
      const selectedTimes: string[] = selectedTime.split(":");
      this.formGroup.get("startingTime").value.setHours(+selectedTimes[0], +selectedTimes[1]);
    }
  }

  getTime(): string {
    const minutes = (this.formGroup.get("startingTime").value.getMinutes()<10?'0':'')+this.formGroup.get("startingTime").value.getMinutes();
    return this.formGroup.get("startingTime").value.getHours() + ":" + minutes;
  }

  acceptOffer(id: string): void {
    this.service.acceptOffer(id, true).subscribe(data => console.log(data));
  }
}
