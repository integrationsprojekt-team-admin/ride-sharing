package de.fhdw.integrationsproject.ridesharing.controller.dto;


import javax.validation.constraints.Positive;

public class LocationDTO {

	@Positive
	public Double lng;

	@Positive
	public Double lat;

	public LocationDTO(@Positive Double lng, @Positive Double lat) {
		this.lng = lng;
		this.lat = lat;
	}

	public LocationDTO() {
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}
}
