package de.fhdw.integrationsproject.ridesharing.controller;

import de.fhdw.integrationsproject.ridesharing.controller.dto.VehicleDTO;
import de.fhdw.integrationsproject.ridesharing.controller.mapper.VehicleMapper;
import de.fhdw.integrationsproject.ridesharing.security.annotations.IsAdminUser;
import de.fhdw.integrationsproject.ridesharing.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Represents a controller class to manage HTTP request for vehicle related operations.
 */
@RestController
@RequestMapping("/vehicles")
@IsAdminUser
class VehicleController {

	/**
	 * Injected vehicle service to which delegation is made.
	 */
	@Autowired
	private VehicleService service;

	/**
	 * Injected vehicle mapper.
	 */
	@Autowired
	private VehicleMapper vehicleMapper;

	/**
	 * GET HTTP request for all saved vehicles.
	 *
	 * @return list of vehicleDTO's
	 */
	@GetMapping
	public ResponseEntity<List<VehicleDTO>> getAll() {
		return new ResponseEntity<>(service.getAll().stream()
				.map(x -> this.vehicleMapper.toVehicleDTO(x))
				.collect(Collectors.toList()), HttpStatus.OK);
	}

	/**
	 * GET HTTP request for a specific vehicle
	 *
	 * @param id to identify the desired vehicle
	 * @return a specific vehicleDTO
	 */
	@GetMapping(path = "/{id}")
	public ResponseEntity<VehicleDTO> getById(@PathVariable("id") UUID id) {
		var vehicle = service.getById(id);
		return vehicle.map(value -> new ResponseEntity<>(this.vehicleMapper.toVehicleDTO(value), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * POST HTTP request for creating a new vehicle
	 *
	 * @param vehicleDTO which should be created
	 * @return a HTTP status code
	 */
	@PostMapping(consumes = "application/json")
	public ResponseEntity<HttpStatus> create(@RequestBody @Valid VehicleDTO vehicleDTO) {
		service.create(this.vehicleMapper.toVehicle(vehicleDTO));
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * DELETE HTTP request for deleting a specific vehicle
	 *
	 * @param id to identify the desired vehicle
	 * @return a HTTP status code
	 */
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable("id") UUID id) {
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
