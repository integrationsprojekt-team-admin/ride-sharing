import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { VehicleDTO } from '../../../services/transport/VehicleDTO';
import { VehicleService } from '../../../services/vehicle.service';

@Component({
  selector: 'app-vehicle-delete',
  templateUrl: './vehicle-delete.component.html',
  styleUrls: ['./vehicle-delete.component.css']
})
export class VehicleDeleteComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<VehicleDeleteComponent>,
    @Inject(MAT_DIALOG_DATA) public vehicle: VehicleDTO,
    private vehicleService: VehicleService,
  ) {
  }

  ngOnInit(): void {
  }

  abort(): void {
    this.dialogRef.close();
  }

  delete(): void {
    this.vehicleService.delete(this.vehicle);
    this.dialogRef.close();
  }

}
