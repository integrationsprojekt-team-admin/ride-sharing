package de.fhdw.integrationsproject.ridesharing.model.offerState;

import de.fhdw.integrationsproject.ridesharing.exceptions.RideOfferIllegalStateException;

public enum RideOfferState {

	RideOfferStateAccepted {
		@Override
		public RideOfferState accept() throws RideOfferIllegalStateException {
			throw new RideOfferIllegalStateException("The offer has already been accepted!");
		}

		@Override
		public RideOfferState decline() throws RideOfferIllegalStateException {
			throw new RideOfferIllegalStateException("An already accepted offer cannot be declined!");
		}
	},
	RideOfferStateDeclined {
		@Override
		public RideOfferState accept() throws RideOfferIllegalStateException {
			throw new RideOfferIllegalStateException("An already declined offer cannot be accepted!");
		}

		@Override
		public RideOfferState decline() throws RideOfferIllegalStateException {
			throw new RideOfferIllegalStateException("The offer has already been declined!");
		}
	},
	RideOfferStateOffered {
		@Override
		public RideOfferState accept() {
			return RideOfferStateAccepted;
		}

		@Override
		public RideOfferState decline() {
			return RideOfferStateDeclined;
		}
	};

	/**
	 * Transition the state machine for input "accept"
	 *
	 * @param The ride offer, whose state is represented by the state machine
	 * @throws RideOfferIllegalStateException
	 */
	abstract public RideOfferState accept() throws RideOfferIllegalStateException;

	/**
	 * Transition the state machine for input "decline"
	 *
	 * @param offer The ride offer, whose state is represented by the state machine
	 * @throws RideOfferIllegalStateException
	 */
	abstract public RideOfferState decline() throws RideOfferIllegalStateException;

}
