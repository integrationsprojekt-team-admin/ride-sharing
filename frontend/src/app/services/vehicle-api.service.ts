import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VehicleDTO } from './transport/VehicleDTO';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VehicleApiService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getAll(): Observable<VehicleDTO[]> {
    return this.httpClient.get<VehicleDTO[]>(`${environment.baseUrl}vehicles/`)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  create(vehicle: VehicleDTO): Observable<any> {
    return this.httpClient.post(`${environment.baseUrl}vehicles/`, vehicle)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  edit(vehicle: VehicleDTO): Observable<any> {
    return this.httpClient.put(`${environment.baseUrl}vehicles/${vehicle.id}`, vehicle)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  delete(vehicle: VehicleDTO): Observable<any> {
    return this.httpClient.delete(`${environment.baseUrl}vehicles/${vehicle.id}`)
      .pipe(
        catchError(this.errorHandler)
      );
  }

  errorHandler(error): Observable<VehicleDTO[]> {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    alert(errorMessage);
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
