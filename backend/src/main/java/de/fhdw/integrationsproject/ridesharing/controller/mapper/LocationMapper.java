package de.fhdw.integrationsproject.ridesharing.controller.mapper;

import de.fhdw.integrationsproject.ridesharing.controller.dto.LocationDTO;
import de.fhdw.integrationsproject.ridesharing.model.Location;
import de.fhdw.integrationsproject.ridesharing.service.LocationService;

public class LocationMapper {

	public static LocationDTO mapToDTO(Location location) {
		return new LocationDTO(location.getLng(), location.getLat());
	}

	public static Location mapToObject(String locationDTO) {
		return LocationService.getLocation(locationDTO);
	}
}
