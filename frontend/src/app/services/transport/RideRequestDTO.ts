import { LocationDTO } from './LocationDTO';

export class RideRequestDTO {
	numberOfPersons: number;
	startingPlace: string;
	targetDestination: string;
	startingTime: Date;
}
