package de.fhdw.integrationsproject.ridesharing.exceptions;

public class RideOfferIllegalStateException extends RideSharingException {

	public RideOfferIllegalStateException(String message) {
		super(message);
	}

}
